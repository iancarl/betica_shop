
$(document).ready(function(){
 let info = {
  "items": {
    "c001": {
      "parent": "c000",
      "title": "Phones"
    },
    "c002": {
      "parent": "c000",
      "title": "Laptops"
    },
    "c003": {
      "parent": "c000",
      "title": "Accessories"
    },
    "c004": {
      "parent": "c100",
      "title": "Cameras"
    },
    "p001": {
      "parent": "c001",
      "manufacturer": "Apple",
      "title": "iPhone 10",
      "price": 499,
      "releaseDate": "2018-04-23T18:25:43.511Z"
    },
    "p002": {
      "parent": "c002",
      "manufacturer": "Apple",
      "title": "Macbook Pro 2017",
      "price": 1999,
      "releaseDate": "2017-04-23T18:25:43.511Z"
    },
    "p003": {
      "parent": "c002",
      "manufacturer": "Dell",
      "title": "XPS 13",
      "price": 1399,
      "releaseDate": "2017-04-23T18:25:43.511Z"
    },
    "p004": {
      "parent": "c001",
      "manufacturer": "Samsung",
      "title": "Samsung Galaxy S10",
      "price": 399,
      "releaseDate": "2017-04-23T18:25:43.511Z"
    },
    "p005": {
      "parent": "c003",
      "manufacturer": "Apple",
      "title": "Apple Charger 12W",
      "price": 99,
      "releaseDate": "2016-12-23T18:25:43.511Z"
    },
    "p006": {
      "parent": "c003",
      "manufacturer": "Samsung",
      "title": "Samsung Charger 12W",
      "price": 89,
      "releaseDate": "2017-04-23T18:25:43.511Z"  
    },
    "p007": {
      "parent": "c005",
      "manufacturer": "Samsung",
      "title": "Samsung VR Headset",
      "price": 699,
      "releaseDate": "2018-04-23T18:25:43.511Z"
    }
  }
};
  

  // console.log(info.items.c001.title);

  $('.menu').append(
    `<li class="nav-item">
      <a class="nav-link" data-toggle="pill" href="#menu1">${info.items.c001.title}</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="pill" href="#menu2">${info.items.c002.title}</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="pill" href="#menu3">${info.items.c003.title}</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="pill" href="#menu4">${info.items.c004.title}</a>
    </li>
    `
    );

  $('.menuAll_row').append(`
    <div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p001.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p001.manufacturer}, to be released in ${(info.items.p001.releaseDate).substring(0,4)}</p>
        <button class="btn btn-block">$${info.items.p001.price}</button>
      </div>
    </div>

    <div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p002.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p002.manufacturer}, to be released in ${(info.items.p002.releaseDate).substring(0,4)}</p>
        <button class="btn btn-block">$${info.items.p002.price}</button>
      </div>
    </div>

    <div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p003.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p003.manufacturer}, to be released in ${(info.items.p003.releaseDate).substring(0,4)}</p>
        <button class="btn btn-block">$${info.items.p003.price}</button>
      </div>
    </div>

    <div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p004.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p004.manufacturer}, to be released in ${(info.items.p004.releaseDate).substring(0,4)}</p>
        <button class="btn btn-block">$${info.items.p004.price}</button>
      </div>
    </div>

    <div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p005.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p005.manufacturer}, to be released in ${(info.items.p005.releaseDate).substring(0,4)}</p>
        <button class="btn btn-block">$${info.items.p005.price}</button>
      </div>
    </div>

    <div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p006.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p006.manufacturer}, to be released in ${(info.items.p006.releaseDate).substring(0,4)}</p>
        <button class="btn btn-block">$${info.items.p006.price}</button>
      </div>
    </div>

    <div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p007.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p007.manufacturer}, to be released in ${(info.items.p007.releaseDate).substring(0,4)}</p>
        <button class="btn btn-block">$${info.items.p007.price}</button>
      </div>
    </div>`);

  $('.menu1_row').append(
    `<div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p001.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p001.manufacturer}, to be released in ${(info.items.p001.releaseDate).substring(0,4)}</p>
        <button class="btn btn-block">$${info.items.p001.price}</button>
      </div>
    </div>

    <div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p004.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p004.manufacturer}, to be released in ${(info.items.p004.releaseDate).substring(0,4)}</p>
        <button class="btn btn-block">$${info.items.p004.price}</button>
      </div>
    </div>`
    );

  $('.menu2_row').append(
    `<div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p002.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p002.manufacturer}, to be released in ${(info.items.p002.releaseDate).substring(0,4)}</p>
        <button class="btn btn-block">$${info.items.p002.price}</button>
      </div>
    </div>

    <div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p003.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p003.manufacturer}, to be released in ${(info.items.p003.releaseDate).substring(0,4)}</p>
        <button class="btn btn-block">$${info.items.p003.price}</button>
      </div>
    </div>`
    );        

    $('.menu3_row').append(
    `<div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p005.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p005.manufacturer}, to be released in ${(info.items.p005.releaseDate).substring(0,4)}</p>

        <button class="btn btn-block">$${info.items.p005.price}</button>
        
      </div>
    </div>

    <div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p006.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p006.manufacturer}, to be released in ${(info.items.p006.releaseDate).substring(0,4)}</p>

        <button class="btn btn-block">$${info.items.p006.price}</button>
        
      </div>
    </div>

    <div class="card col-md-3 mt-3 mr-2">
      <div class="card-header">
        ${info.items.p007.title}
      </div>
      <div class="card-body">
        <p><span class="font-weight-bold text-muted">Manufacturered by</span> ${info.items.p007.manufacturer}, to be released in ${(info.items.p007.releaseDate).substring(0,4)}</p>

        <button class="btn btn-block">$${info.items.p007.price}</button>
        
      </div>
    </div>`
    );        

        

});